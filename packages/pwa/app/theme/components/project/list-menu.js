/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */
export default {
    baseStyle: {
        container: {
            minWidth: 'xs',
            width: 'full',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'flex-start',
            paddingLeft: 4,
            display: {base: 'none', lg: 'flex'}
        },
        stackContainer: {
            whiteSpace: 'nowrap',
            flexWrap: 'wrap'
        },
        popoverContent: {
            marginLeft: 'auto',
            marginRight: 'auto',
            border: 0,
            boxShadow: 'xl',
            paddingTop: 3,
            paddingRight: 4,
            paddingBottom: 4,
            paddingLeft: 4,
            minWidth: '100%',
            borderRadius: 0,
            position: 'absolute'
        },
        popoverContainer: {
            paddingTop: 0,
            paddingBottom: 8,
            maxWidth: 'container.xxxl'
        },
        listMenuTriggerContainer: {
            display: 'flex',
            alignItems: 'stretch',
            _hover: {
                backgroundColor: '#fff',
                color: '#000'
            }
        },
        listMenuTriggerLink: {
            display: 'flex',
            alignItems: 'center',
            whiteSpace: 'nowrap',
            position: 'relative',
            paddingTop: 0,
            paddingRight: 0,
            paddingBottom: 0,
            paddingLeft: 3,
            fontSize: 'md',
            fontWeight: 700,
            color: '#fff',
            _hover: {
                textDecoration: 'none'
            }
        },
        listMenuTriggerLinkWithIcon: {paddingRight: 3},
        listMenuTriggerLinkActive: {
            backgroundColor: '#fff',
            color: '#000',
            textDecoration: 'none'
        },
        listMenuTriggerLinkIcon: {
            marginTop: 3,
            marginRight: 3,
            marginBottom: 2,
            marginLeft: 0,
            _hover: {
                textDecoration: 'none'
            }
        },
        listMenuPopoverItem: {
            display: 'flex',
            alignItems: 'stretch'
        }
    },
    parts: [
        'container',
        'stackContainer',
        'popoverContent',
        'popoverContainer',
        'listMenuTriggerContainer',
        'listMenuTriggerLink',
        'listMenuTriggerLinkActive',
        'listMenuTriggerIcon'
    ]
}
