/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */

import React from 'react'
import {Box, Heading, Flex, Button, Stack, Text} from '@chakra-ui/react'
import PropTypes from 'prop-types'
import {Helmet} from 'react-helmet'
import {useIntl} from 'react-intl'
import Link from '../../components/link'
import contentfulApi from '../../contentful-api'

const StaticPage = ({title, description}) => {
    const intl = useIntl()
    return (
        <Box
            layerStyle="page"
            className="page-not-found"
            height={'100%'}
            padding={{lg: 8, md: 6, sm: 0, base: 0}}
        >
            <Helmet>
                <title>
                    {intl.formatMessage({
                        defaultMessage: "It's a static page",
                        id: ''
                    })}
                </title>
            </Helmet>

            <Flex
                h="100%"
                justify="center"
                align="center"
                flexDirection="column"
                px={{base: 5, md: 12}}
                py={{base: 48, md: 60}}
            >
                <Heading as="h2" fontSize={['xl', '2xl', '2xl', '3xl']} mb={2} align="center">
                    {title}
                </Heading>

                <Text mb={4} align="center">
                    {description}
                </Text>

                <Stack direction={['column', 'row']} width={['100%', 'auto']}>
                    <Button as={Link} to={'/'}>
                        {intl.formatMessage({
                            defaultMessage: 'Go to home page',
                            id: 'page_not_found.link.homepage'
                        })}
                    </Button>
                </Stack>
            </Flex>
        </Box>
    )
}

StaticPage.shouldGetProps = ({previousLocation, location}) => {
    return previousLocation?.pathname !== location.pathname
}

StaticPage.getProps = async ({res}) => {
    const data = await contentfulApi.staticPageContent()

    if (res) {
        res.set('Cache-Control', 'max-age=9000')
    }

    const {title, description} = data?.fields
    return {title, description}
}

StaticPage.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string
}

export default StaticPage
