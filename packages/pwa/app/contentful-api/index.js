/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */
import {createContentfulFetch} from './utils'

class ContentfulApi {
    constructor() {
        this.fetch = createContentfulFetch()
    }

    async staticPageContent() {
        return await this.fetch('5SW4MxcRcM2MqGlPO0Xf1D')
    }

    async fetchAsset(id) {
        const {includes} = await this.fetch()
        let asset
        includes.Asset.map((item) => {
            if (id === item.fields.title) {
                asset = item.fields.file
            }
        })
        return asset
    }

    async fetchResource(id) {
        const {fields} = await this.fetch(id)
        return fields
    }
}

export default new ContentfulApi()
