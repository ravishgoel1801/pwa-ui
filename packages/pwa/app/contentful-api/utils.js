/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */
import axios from 'axios'

// This function is used to interact with the CONTENTFUL API
export const createContentfulFetch = () => async (resourceId) => {
    const host = `https://cdn.contentful.com`

    const {data} = await axios.get(
        `${host}/spaces/4nyg7pl7kt91/environments/master/entries/${resourceId ? resourceId : ''}`,

        {
            params: {
                access_token: '4tTl-I76Lz9AIUj_t2qID4SPXkCpe9y7Fly3_yiX0_U'
            }
        }
    )

    return data
}
