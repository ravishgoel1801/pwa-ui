/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */
import contentfulInstance from '../index'
import {useState, useEffect, useCallback} from 'react'
export const useAsset = (id) => {
    const [state, setState] = useState({isAssetLoading: true, asset: {}})

    const runQuery = useCallback(async () => {
        const asset = await contentfulInstance.fetchAsset(id)
        setState({isAssetLoading: false, asset})
    }, [id])

    useEffect(() => {
        runQuery()
    }, [runQuery])

    return state
}
