/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */
import contentfulInstance from '../index'
import {useState, useEffect, useCallback} from 'react'
export const useFeatures = (id) => {
    const [state, setState] = useState({isFeaturesLoading: true, features: []})

    const runQuery = useCallback(async () => {
        const {items} = await contentfulInstance.fetchResource(id)
        let tempArray = []
        items.map(async (item, index) => {
            const feature = await contentfulInstance.fetchResource(item?.sys?.id)
            tempArray.push(feature)
            if (index === items.length - 1) {
                setState({
                    isFeaturesLoading: false,
                    features: [...tempArray]
                })
            }
        })
    }, [id])

    useEffect(() => {
        runQuery()
    }, [runQuery])

    return state
}
