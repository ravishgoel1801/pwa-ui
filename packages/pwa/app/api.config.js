/*
 * Copyright (c) 2021, salesforce.com, inc.
 * All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause
 * For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
 */
export const commerceAPIConfig = {
    proxyPath: `/mobify/proxy/api`,
    parameters: {
        clientId: 'add11732-f6bb-4833-997f-6853f4fc6619',
        organizationId: 'f_ecom_zzuw_004',
        shortCode: 'kv7kzm78',
        siteId: 'RefArch'
    }
}
export const einsteinAPIConfig = {
    proxyPath: `/mobify/proxy/einstein`,
    einsteinId: '1ea06c6e-c936-4324-bcf0-fada93f83bb1',
    // This differs from the siteId in commerceAPIConfig for testing purposes
    siteId: 'aaij-MobileFirst'
}
